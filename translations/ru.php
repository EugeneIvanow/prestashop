<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{my_test_module}prestashop>my_test_module_235db12c3993423294d4fef5771f3ee7'] = 'Тестовый модуль';
$_MODULE['<{my_test_module}prestashop>my_test_module_fe7228af1f78ba9e79ae97c5dae07855'] = ' Это тестовый модуль';
$_MODULE['<{my_test_module}prestashop>my_test_module_a3882a93383984924deb1e23e39be441'] = 'Цена от не указана';
$_MODULE['<{my_test_module}prestashop>my_test_module_4b4016b4088346bedc466fa642aa2507'] = 'Цена от не указана';
$_MODULE['<{my_test_module}prestashop>my_test_module_bbc318cb9df0c9d20c8d4c9b31dc1e80'] = 'Цена до не указана';
$_MODULE['<{my_test_module}prestashop>my_test_module_9ebd868c6f9059db9e2375dc39a6a6a8'] = 'Цена от не может быть больше цены до';
$_MODULE['<{my_test_module}prestashop>my_test_module_d4b764e109c6721dab6ba130e056fa52'] = 'Цена от';
$_MODULE['<{my_test_module}prestashop>my_test_module_e160f0f778bc3d890a5faf131aad6ba9'] = 'Укажите цену от';
$_MODULE['<{my_test_module}prestashop>my_test_module_e1e6b888280354792e8c8d782e5a8ff5'] = 'Цена до';
$_MODULE['<{my_test_module}prestashop>my_test_module_ea87884f8d05b13dd6457a3bf9cf04e3'] = 'Укажите цену до';
$_MODULE['<{my_test_module}prestashop>my_test_module_f7ea4e9a3dd7d28e379583eabcfa4a56'] = 'В указанном ценовом диапазоне %s - %s в магазине находится %s  товаров';
